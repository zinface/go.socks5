package socks5

import (
	"io"
	"log"
)

type ClientAuthMessage struct {
	Version  byte
	NMethods byte
	Methods  []Method
}

func NewClientAuthMessage(conn io.Reader) (*ClientAuthMessage, error) {
	// 读取版本
	buf := make([]byte, 2)

	_, err := io.ReadFull(conn, buf)
	if err != nil {
		return nil, err
	}
	log.Printf(">>> auth version: socks%v", buf[0])

	// 验证版本
	if buf[0] != SOCKS5_VERSION {
		return nil, ErrorVersionNotSupported
	}

	// 读取 methods
	nmethods := buf[1]
	buf = make([]byte, nmethods)

	_, err = io.ReadFull(conn, buf)
	if err != nil {
		return nil, err
	}

	return &ClientAuthMessage{
		Version:  SOCKS5_VERSION,
		NMethods: nmethods,
		Methods:  buf,
	}, nil
}

func NewServerAuthMessage(conn io.Writer, method Method) error {
	buf := []byte{SOCKS5_VERSION, method}
	_, err := conn.Write(buf)
	return err
}

type Method = byte

const (
	MethodNoAuth       Method = 0x00
	MethodGSSAPI       Method = 0x01
	MethodPassword     Method = 0x02
	MethodNoAcceptable Method = 0xff
)

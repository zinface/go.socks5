package socks5

import (
	"bytes"
	"testing"
)

func TestClientRequestMessage(t *testing.T) {
	tests := []struct {
		Version  byte
		Cmd      Command
		AddrType AddressType
		Address  []byte
		Port     []byte
		Error    error
		Message  ClientRequestMessage
	}{
		{
			Version:  SOCKS5_VERSION,
			Cmd:      CmdConnect,
			AddrType: TypeIPv4,
			Address:  []byte{123, 35, 13, 89},
			Port:     []byte{0x00, 0x50},
			Error:    nil,
			Message: ClientRequestMessage{
				Cmd:      CmdConnect,
				AddrType: TypeIPv4,
				Address:  "123.35.13.89",
				Port:     0x0050,
			},
		},
		{
			Version:  0x00,
			Cmd:      CmdConnect,
			AddrType: TypeIPv4,
			Address:  []byte{123, 35, 13, 89},
			Port:     []byte{0x00, 0x50},
			Error:    ErrorVersionNotSupported,
			Message: ClientRequestMessage{
				Cmd:      CmdConnect,
				AddrType: TypeIPv4,
				Address:  "123.35.13.89",
				Port:     0x0050,
			},
		},
	}

	for _, test := range tests {
		var buf bytes.Buffer
		buf.Write([]byte{test.Version, test.Cmd, REVERSED_FIELD, test.AddrType})
		buf.Write(test.Address)
		buf.Write(test.Port)

		message, err := NewClientRequestMessage(&buf)
		if err != test.Error {
			t.Fatalf("shout get error %s, but got %s\n", test.Error, err)
		}

		if err != nil {
			continue
		}

		if *message != test.Message {
			t.Fatalf("shout get message %v, but got %v\n", test.Message, *message)
		}
	}
}

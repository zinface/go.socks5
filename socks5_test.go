package socks5

import (
	"bytes"
	"net"
	"reflect"
	"testing"
)

func TestWriteRequestSuccessMessage(t *testing.T) {
	// t.Run("write valid request success",)
	var buf bytes.Buffer
	ip := net.IP([]byte{123, 123, 11, 11})

	err := WriteRequestSuccessMessage(&buf, ip, 1081)
	if err != nil {
		t.Fatalf("error while writing: %s", err.Error())
	}

	want := []byte{SOCKS5_VERSION, ReplySuccess, REVERSED_FIELD, TypeIPv4, 123, 123, 11, 11, 0x04, 0x39}
	got := buf.Bytes()
	if !reflect.DeepEqual(got, want) {
		t.Fatalf("message not match: %#v, want %#v", got, want)
	}
}

func TestWriteRequestFailureMessage(t *testing.T) {

}

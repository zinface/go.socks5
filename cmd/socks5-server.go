package main

import (
	"log"

	socks5 "gitee.com/zinface/go.socks5"
)

func main() {
	server := socks5.Socks5Server{
		IP:   "0.0.0.0",
		Port: 1080,
	}

	log.Printf("starting server: %v:%v\n", server.IP, server.Port)
	err := server.Run()
	if err != nil {
		log.Fatal(err)
	}
}

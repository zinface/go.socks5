package socks5

import (
	"io"
	"net"
)

const (
	IPv4Length = 4
	IPv6Length = 16
	PortLength = 2
)

type ClientRequestMessage struct {
	// Version  byte
	Cmd      Command
	AddrType AddressType
	Address  string
	Port     uint16
}

type Command = byte

const (
	CmdConnect Command = 0x01
	CmdBind    Command = 0x02
	CmdUDP     Command = 0x3
)

type AddressType = byte

const (
	TypeIPv4   AddressType = 0x01
	TypeDomain AddressType = 0x03
	TypeIPv6   AddressType = 0x04
)

type ReplyType = byte

const (
	ReplySuccess ReplyType = iota
	ReplyServerFailure
	ReplyConnectNotAllowed
	ReplyNetworkUnreachable
	ReplyHostUnreachable
	ReplyConnectionRefused
	ReplyTTLException
	ReplyCommandNotSupported
	ReplyAddressTypeNotSupported
)

func NewClientRequestMessage(conn io.Reader) (*ClientRequestMessage, error) {
	// Version, Command, Reserved
	buf := make([]byte, 4)
	if _, err := io.ReadFull(conn, buf); err != nil {
		return nil, err
	}

	// check
	version, command, reserved, addressType := buf[0], buf[1], buf[2], buf[3]
	if version != SOCKS5_VERSION {
		return nil, ErrorVersionNotSupported
	}
	if command != CmdConnect && command != CmdBind && command != CmdUDP {
		return nil, ErrorCommandNotSupported
	}
	if reserved != REVERSED_FIELD {
		return nil, ErrorReservedFieldInvalid
	}
	if addressType != TypeIPv4 && addressType != TypeIPv6 && addressType != TypeDomain {
		return nil, ErrorAddressTypeNotSupported
	}

	message := ClientRequestMessage{
		Cmd:      command,
		AddrType: addressType,
	}

	switch addressType {
	case TypeIPv6:
		buf = make([]byte, IPv6Length)
		fallthrough
	case TypeIPv4:
		if _, err := io.ReadFull(conn, buf); err != nil {
			return nil, err
		}
		ip := net.IP(buf)
		message.Address = ip.String()
	case TypeDomain:
		if _, err := io.ReadFull(conn, buf[:1]); err != nil {
			return nil, err
		}
		domainLength := buf[0]
		if domainLength > IPv4Length {
			buf = make([]byte, domainLength)
		}
		if _, err := io.ReadFull(conn, buf[:domainLength]); err != nil {
			return nil, err
		}
		message.Address = string(buf[:domainLength])
	}

	if _, err := io.ReadFull(conn, buf[:PortLength]); err != nil {
		return nil, err
	}

	message.Port = (uint16(buf[0]) << 8) + uint16(buf[1])

	return &message, nil
}

func WriteRequestSuccessMessage(conn io.Writer, ip net.IP, port uint16) error {
	addressType := TypeIPv4
	if len(ip) == IPv6Length {
		addressType = TypeIPv6
	}

	// Write Version, ReplyType, Reserved, addressType,
	_, err := conn.Write([]byte{SOCKS5_VERSION, ReplySuccess, REVERSED_FIELD, addressType})
	if err != nil {
		return err
	}

	// Write bind IP(IPv4/IPv6)
	if _, err := conn.Write(ip); err != nil {
		return err
	}

	// Write bind Port
	// if _, err := conn.Write(port)
	buf := make([]byte, 2)
	buf[0] = byte(port >> 8)
	buf[1] = byte(port - uint16(buf[0])<<8)
	_, err = conn.Write(buf)
	return err

}

func WriteRequestFailureMessage(conn io.Writer, replyType ReplyType) error {
	_, err := conn.Write([]byte{SOCKS5_VERSION, replyType, REVERSED_FIELD, TypeIPv4, 0, 0, 0, 0, 0, 0})
	return err

}

package socks5

import (
	"bytes"
	"reflect"
	"testing"
)

func TestNewClientAuthMessage(t *testing.T) {
	t.Run("should generate a new client auth message", func(t *testing.T) {
		b := []byte{SOCKS5_VERSION, 2, MethodNoAuth, MethodGSSAPI}
		r := bytes.NewReader(b)

		message, err := NewClientAuthMessage(r)
		if err != nil {
			t.Fatalf("NewClientAuthMessage failed, err: %v", err)
		}

		if message.Version != SOCKS5_VERSION {
			t.Fatalf("version mismatch for %d", message.Version)
		}

		if message.NMethods != 2 {
			t.Fatalf("nmethods mismatch for %d", message.NMethods)
		}

		if reflect.DeepEqual(message.NMethods, []byte{MethodNoAuth, MethodGSSAPI}) {
			t.Fatalf("methods: %v , but mismatch for %d", []byte{MethodNoAuth, MethodGSSAPI}, message.Methods)
		}
	})

	t.Run("methods length is shorter than nmethods", func(t *testing.T) {
		b := []byte{SOCKS5_VERSION, 2, MethodNoAuth}
		r := bytes.NewReader(b)

		_, err := NewClientAuthMessage(r)
		if err == nil {
			t.Fatalf("should get error != nil but got nil")
		}
	})
}

func TestNewServerAuth(t *testing.T) {
	t.Run("should pass", func(t *testing.T) {
		var buf bytes.Buffer
		err := NewServerAuthMessage(&buf, MethodNoAuth)
		if err != nil {
			t.Fatalf("should get error == nil but got: %s", err)
		}

		got := buf.Bytes()
		if !reflect.DeepEqual(got, []byte{SOCKS5_VERSION, MethodNoAuth}) {
			t.Fatalf("should send %v, but send %v", []byte{SOCKS5_VERSION, MethodNoAuth}, got)
		}
	})
}

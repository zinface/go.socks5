all: linux windows macos
	@echo builded

build:
	go build -o bin/socks5-server cmd/socks5-server.go

install:
	go install cmd/socks5-server.go

windows:
	GOOS=windows GOARCH=amd64 go build -o bin/socks5-windows.exe cmd/socks5-server.go

linux-all: linux-amd64 linux-arm linux-arm64

linux-amd64:
	GOOS=linux GOARCH=amd64 go build -o bin/socks5-linux-amd64 cmd/socks5-server.go

linux-arm:
	GOOS=linux GOARCH=arm go build -o bin/socks5-linux-arm cmd/socks5-server.go

linux-arm64:
	GOOS=linux GOARCH=arm64 go build -o bin/socks5-linux-arm64 cmd/socks5-server.go

macos:
	GOOS=darwin GOARCH=amd64 go build -o bin/socks5-darwin cmd/socks5-server.go

# go.socks5

> 这是在拥有特定场景情况下使用的socks5服务程序

- 场景1:
    1. 有个只能 windows 连的 vpn
    2. 开个windows，把 socks5 代理服务一放上去
    3. 然后我在我的linux机器上用 socks5 代理上网


- 这是 Makefile 内容
    1. 你可以执行 make 构建出所有目标机器的可执行文件
    2. 你可以执行 make linux 来只构建 linux amd64(x86_64) 架构的程序
    
        ```makefile
        all: linux windows macos
            @echo builded

        windows:
            GOOS=windows GOARCH=amd64 go build -o bin/socks5-windows.exe cmd/main.go

        linux:
            GOOS=linux GOARCH=amd64 go build -o bin/socks5-linux cmd/main.go

        macos:
            GOOS=darwin GOARCH=amd64 go build -o bin/socks5-darwin cmd/main.go
        ```

- 已实现部分
    1. 基于 tcp socket， 也仅实现了 tcp 代理
    2. 使用无验证方式，虽然协议中有各种验证方案
    3. 相比原始从 [BV1qA4y1D74u](https://www.bilibili.com/video/BV1qA4y1D74u) 视频中增加了坑爆破访问，也就是一些小细节处。 